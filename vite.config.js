import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { VitePWA } from "vite-plugin-pwa";

export default defineConfig({
  plugins: [
    react(),
    VitePWA({
      registerType: "autoUpdate",
      injectRegister: "inline",
      devOptions: {
        enabled: true,
      },

      workbox: {
        runtimeCaching: [
          {
            urlPattern: /^https:\/\/api\.neshan\.org\/v1\/search\?term=.*&lat=.*&lng=.*/,
            handler: "NetworkFirst", // Use 'NetworkFirst' or other strategy as needed
            options: {
              cacheName: "location-data-cache",
              expiration: {
                maxEntries: 50,
                maxAgeSeconds: 60 * 60 * 24, // 1 day
              },
            },
          },
        ],
        globPatterns: ["**/*.{js,css,html,ico,png,svg}"],
      },
    }),
  ],
});
