import mapboxgl from "mapbox-gl";
export function createMarker(position, map, markerRef, setMarkerRef) {
  console.log(position, map);
  if (markerRef) markerRef.remove();

  const marker = new mapboxgl.Marker({
    color: "#1fd6be",
    draggable: true,
  })
    .setLngLat(position)
    .addTo(map);
  map.setCenter(position);

  setMarkerRef(marker);
  return marker;
}
