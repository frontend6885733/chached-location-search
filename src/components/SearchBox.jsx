import { useState } from "react";
import useFetchLocation from "../hooks/useFetchLocation";
import ResultItem from "./resultItem";
import "./searchBox.css";
function SearchBox({ getLocation }) {
  const [locationName, setLocationName] = useState("");
  const { location, isLoading } = useFetchLocation(locationName);
  console.log(location);
  return (
    <div className="search-box">
      <div className="search-field">
        <input
          type="text"
          placeholder="جستجو..."
          onChange={(e) => setLocationName(e.target.value)}
        />
      </div>
      <div className="results">
        {location &&
          location.map((loc, i) => (
            <ResultItem key={i} loc={loc} getLocation={getLocation} />
          ))}
        {isLoading && <span className="loading">در حال بارگذاری ...</span>}
      </div>
    </div>
  );
}

export default SearchBox;
