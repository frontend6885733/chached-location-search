import "./resultItem.css";
function ResultItem({ loc, getLocation }) {
  return (
    <>
      <div
        className="result-item"
        onClick={() => {
          console.log("s");
          getLocation([loc.cord.x, loc.cord.y]);
        }}
      >
        <span className="title">{loc.title}</span>
        <span className="subtitle-result">{loc.address}</span>
      </div>
      <hr className="line" />
    </>
  );
}

export default ResultItem;
