import { useRef, useEffect, useState } from "react";
import "./App.css";
import SearchBox from "./components/SearchBox";
import mapboxgl from "mapbox-gl";
import { createMarker } from "./createMarker";
mapboxgl.accessToken =
  "pk.eyJ1IjoibW9zYTU0NDUiLCJhIjoiY2p3a3ppcjY1MHA1ZTQ0cDk5MDU1eXNxOCJ9.psGVr3_kzZWMk--4Ojbhdw";

function App() {
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [markerRef, setMarkerRef] = useState(null);
  const getLocation = (cord) => {
    console.log(cord);
    createMarker(cord, map.current, markerRef, setMarkerRef);
  };

  useEffect(() => {
    if (!map.current) {
      map.current = new mapboxgl.Map({
        container: "map",
        style: "mapbox://styles/mapbox/streets-v12",
        center: [59.578749, 36.313287],
        zoom: 10,
      });
    }
  }, []);
  return (
    <div>
      <SearchBox getLocation={getLocation} />
      <div ref={mapContainer} className="map-container" id="map" />
    </div>
  );
}

export default App;
