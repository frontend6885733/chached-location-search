// import { useEffect, useState } from "react";

// export default function useFetchLocation(locationName) {
//   const [location, setLocation] = useState([]);

//   useEffect(() => {
//     // Check if the browser supports the Cache API.
//     if ("caches" in window) {
//       // Try to fetch data from the network first.
//       fetchLocationFromNetwork(locationName)
//         .then((data) => {
//           setLocation(data);
//           // Cache the fetched data for offline use.
//           cacheLocationData(locationName, data);
//         })
//         .catch(() => {
//           // If fetching from the network fails, try to get data from the cache.
//           getLocationDataFromCache(locationName).then((cachedData) => {
//             setLocation(cachedData);
//           });
//         });
//     }
//   }, [locationName]);

//   async function fetchLocationFromNetwork(locationName) {
//     try {
//       const response = await fetch(
//         `https://api.neshan.org/v1/search?term=${locationName}&lat=36.313287&lng=59.578749`,
//         {
//           method: "GET",
//           headers: { "Api-Key": "service.c4a9a068ce75442ca74d0dcf6b1a678d" },
//         }
//       );

//       if (!response.ok) {
//         throw new Error("Network response was not ok");
//       }

//       const data = await response.json();
//       return data.items.map((loc) => ({
//         title: loc.title,
//         address: loc.address,
//         cord: { x: loc.location.x, y: loc.location.y },
//       }));
//     } catch (error) {
//       throw error;
//     }
//   }

//   async function cacheLocationData(locationName, data) {
//     try {
//       const cache = await caches.open("location-data-cache");
//       const cacheKey = `location-${locationName}`;
//       const cacheData = new Response(JSON.stringify(data));
//       await cache.put(cacheKey, cacheData);
//     } catch (error) {
//       console.error("Error caching location data:", error);
//     }
//   }

//   async function getLocationDataFromCache(locationName) {
//     try {
//       const cache = await caches.open("location-data-cache");
//       const cacheKey = `location-${locationName}`;
//       const response = await cache.match(cacheKey);

//       if (response) {
//         const cachedData = await response.json();
//         return cachedData;
//       } else {
//         return [];
//       }
//     } catch (error) {
//       console.error("Error getting location data from cache:", error);
//       return [];
//     }
//   }

//   return { location };
// }

import { useEffect, useState } from "react";

export default function useFetchLocation(locationName) {
  const [location, setLocation] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    async function fetchData() {
      try {
        setIsLoading(true);
        const response = await fetch(
          `https://api.neshan.org/v1/search?term=${locationName}&lat=36.313287&lng=59.578749`,
          {
            method: "GET",
            headers: { "Api-Key": "service.c4a9a068ce75442ca74d0dcf6b1a678d" },
          }
        );

        if (!response.ok) {
          throw new Error("Network response was not ok");
        }

        const data = await response.json();
        setLocation(
          data.items.map((loc) => ({
            title: loc.title,
            address: loc.address,
            cord: { x: loc.location.x, y: loc.location.y },
          }))
        );
      } catch (error) {
        console.error("Error fetching location data:", error);
      } finally {
        setIsLoading(false);
      }
    }

    fetchData();
  }, [locationName]);

  return { location, isLoading };
}
